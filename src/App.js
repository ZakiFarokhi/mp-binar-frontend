import React from 'react';
// import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router, Route} from 'react-router-dom'
import Headers from './public/common/header'
import Home from './public/homepage/home'
import PublicProduct from './public/product/product'
import SignUpPage from './public/signup/signup'
import UserPage from './private/user/userpage'
import ProductPage from './public/product/productbyId'
function App() {
  return (
    <div className="App">
      <Router>
        <Headers></Headers>
        <Route exact path='/' component={Home}></Route>
        <Route exact path='/public/product/' component={PublicProduct}></Route>
        <Route exact path='/public/signup/' component={SignUpPage}></Route>
        <Route exact path='/private/user/:id' component={UserPage}></Route>
        <Route exact path='/public/product/:id' component={ProductPage}></Route>
      </Router>
    </div>
  );
}

export default App;
