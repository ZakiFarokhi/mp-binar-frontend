import React from 'react';
import {
  Collapse, Navbar,
  NavbarToggler, NavbarBrand, Nav,
  NavItem, NavLink, Modal, ModalHeader,
  ModalBody, ModalFooter, Form,
  FormGroup, Label, Input, Button
} from 'reactstrap';
import { Link, withRouter } from 'react-router-dom'

class Header extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {

      modal: false,
      user: [],
      username: '',
      password: '',
      isLogin: false,
      redirect: false,
      labelButton: 'login'
    };
  }
  toggle() {
    if (this.state.labelButton === 'logout') {
      localStorage.clear()
      this.props.history.push('/')
      this.setState({
        labelButton: 'login'
      })
    } else if (this.state.labelButton === 'login') {
      this.setState({
        modal: !this.state.modal
      });
    }

  }
  render() {
    return (
      <div>
        <Navbar color="light" light expand="md">
          <NavbarBrand href="/">reactstrap</NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <Button color='secondary' onClick={this.toggle}>{this.state.labelButton}</Button>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                  {this.state.isLogin === true ? <ModalHeader toggle={this.toggle}>{this.state.labelButton}</ModalHeader> : null}
                  <ModalBody>
                    <Form>
                      <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                        <Label className="mr-sm-2">Username</Label>
                        <Input type="text" placeholder="Username" onChange={this.inputUserName} />
                      </FormGroup>
                      <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                        <Label for="examplePassword" className="mr-sm-2" >Password</Label>
                        <Input type="password" name="password" placeholder="password" onChange={this.inputPassword} />
                      </FormGroup>
                    </Form>
                  </ModalBody>
                  <ModalFooter>
                    <Link to="/homepage/">
                      <Button color="primary" onClick={this.login}>Login</Button>
                    </Link>

                    <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                  </ModalFooter>
                </Modal>
              </NavItem>
              <NavItem>
                <NavLink href="/public/product/">Product</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }

  inputUserName = (event) => {
    this.setState({
      username: event.target.value
    })
  }
  inputPassword = (event) => {
    this.setState({
      password: event.target.value
    })
  }
  login = () => {
    return fetch('https://mp-binar-backend.herokuapp.com/user/login', {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, cors, *same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json',
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow', // manual, *follow, error
      referrer: 'no-referrer', // no-referrer, *client
      body: JSON.stringify({
        username: this.state.username,
        password: this.state.password

      }), // body data type must match "Content-Type" header
    })
      .then(response => response.json())
      .then(data => {
          console.log(data)
          localStorage.setItem("isLoggin", true)
          localStorage.setItem("TOKEN", data.data.token)
          localStorage.setItem("USER", data.data.userId)
  
          this.setState({
            modal: !this.state.modal,
            labelButton: 'logout'
          })
          this.props.history.push(`/private/user/${data.data.userId}`)
      })// parses JSON response into native JavaScript objects 

  }
  componentDidMount=()=>{
    if(window.localStorage.getItem('isLoggin')){
      this.setState({
        labelButton:'logout'
      })
    }
  }
}
export default withRouter(Header)
