import React from 'react';
import logo from '../../logo.svg';
import { Jumbotron, Button, CardGroup, CardImg, Container, Row, Col } from 'reactstrap';

const Example = (props) => {
  return (
    <div>
      <Jumbotron>
        <h1 className="display-3">Hello</h1>
        <p className="lead">Welcome to Our MarketPlace Project</p>
        <hr className="my-2" />
        <p>Maybe you can find what you want here</p>
        <p className="lead">
          <Button color="primary" href="/public/product">Our Product</Button>
          <Button style={{margin:"2%"}} color="primary" href="/public/signup">Join Us!!</Button>
        </p>
      </Jumbotron>
       <Container>  
         <Row>
           <Col xs="3">
             <CardGroup>
               <CardImg top width="50%" src={logo}/> 
               <Button >Learn More</Button>
             </CardGroup>
           </Col>
           <Col xs="3">
             <CardGroup>
               <CardImg top width="100%" src={logo}/> 
               <Button>Learn More</Button>
             </CardGroup>
           </Col>
           <Col xs="3">
             <CardGroup>
               <CardImg top width="100%" src={logo}/> 
               <Button>Learn More</Button>
             </CardGroup>
           </Col>
           <Col xs="3">
             <CardGroup>
               <CardImg top width="100%" src={logo}/> 
               <Button>Learn More</Button>
             </CardGroup>
           </Col>
         </Row>
       </Container>
       
    </div>
  );
};

export default Example;
