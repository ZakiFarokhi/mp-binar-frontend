import React from 'react';
import {
  Card, Button, CardImg, CardTitle, CardText, CardGroup,
  CardSubtitle, CardBody, Container, Row, Col
} from 'reactstrap';
export default class Example extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      product: [],
      user: [],
      producId:''
    }
  }
  
  render() {
    return (
      <CardGroup>
        <Container>
          <Row>


            {this.state.product.map((product, index) => (

              <Col xs="2">
                <Card>
                  <CardImg top width="100%" src={product.image ? product.image : `https://ui-avatars.com/api/?name=${product.name}`} alt="Card image cap" />
                  <CardBody>
                    <CardTitle>{product.name}</CardTitle>
                    <CardSubtitle>{product.price}</CardSubtitle>
                    <CardText>{}</CardText>
                    <Button href={`/public/product/${product._id}`}>See Product</Button>
                  </CardBody>
                </Card>
              </Col>
            ))}
          </Row>
        </Container>
      </CardGroup>
    )
  }

  componentDidMount() {
    fetch(`https://mp-binar-backend.herokuapp.com/product/`)
      .then(response => response.json())
      .then(data => {
        console.log(data)
        this.setState({
          product: data.data,
          user: data.data.user
        })
        console.log(this.state.product)
      })
  }

};

