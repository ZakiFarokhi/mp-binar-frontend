import React from 'react';
import {
    TabContent, TabPane, Nav, NavItem,
    NavLink, Card, Button, CardTitle,
    CardText, Row, Col, CardImg, CardBody, CardSubtitle,
    Container, Form, FormGroup, Label, Input, Modal, ModalHeader,
    ModalBody, ModalFooter
} from 'reactstrap';
import classnames from 'classnames';

export default class Example extends React.Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            activeTab: '1',

            product: {},
            reviewProduct: [],

            productName: '',
            productPrice: '',
            productImage: '',
            productId: '',

            redirect: false,

            isDisable: true,

            modal: false,
            modalEditProduct: false
        };
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }
    enable = () => {
        this.setState({
            isDisable: false

        })
    }
    onChangeProductName = (event) => {
        this.setState({
            productName: event.target.value
        })
    }
    onChangeProducPrice = (event) => {
        this.setState({
            productPrice: event.target.value
        })
    }
    Onchangeusername = (event) => {
        this.setState({
            user: { username: event.target.value }
        })
    }
    Onchangepassword = (event) => {
        this.setState({
            user: { password: event.target.value }
        })
    }
    Onchangeemail = (event) => {
        this.setState({
            user: { email: event.target.value }
        })
    }
    edituser = () => {
        fetch(`https://mp-binar-backend.herokuapp.com/user/${this.props.match.params.id}`, {
            method: 'PUT', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, cors, *same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
                // 'Content-Type': 'application/x-www-form-urlencoded',
                'authorization': `${localStorage.getItem('TOKEN')}`
            },
            redirect: 'follow', // manual, *follow, error
            referrer: 'no-referrer', // no-referrer, *client
            body: JSON.stringify({
                username: this.state.user.username,
                password: this.state.user.password,
                email: this.state.user.email,
                // image: "https://ui-avatars.com/api/?name=" + this.state.username
            }), // body data type must match "Content-Type" header
        })
            .then(response => response.json())
            .then(data => {
                console.log(data)
                this.setState({
                    isDisable: true

                })
            }) // parses JSON response into native JavaScript objects

    }
    modalToggle = () => {
        this.setState({
            modal: !this.state.modal
        })
    }
    modalEditProduct = () => {
        this.setState({
            modalEditProduct: !this.state.modalEditProduct
        })
    }
    createProduct = () => {
        fetch(`https://mp-binar-backend.herokuapp.com/product/`, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, cors, *same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
                // 'Content-Type': 'application/x-www-form-urlencoded',
                'authorization': `${localStorage.getItem('TOKEN')}`
            },
            redirect: 'follow', // manual, *follow, error
            referrer: 'no-referrer', // no-referrer, *client
            body: JSON.stringify({
                name: this.state.productName,
                price: this.state.productPrice,
                image: "https://ui-avatars.com/api/?name=" + this.state.productName,
                // image: "https://ui-avatars.com/api/?name=" + this.state.username
            }), // body data type must match "Content-Type" header
        })
            .then(response => response.json())
            .then(data => {
                console.log(data)
                alert('Product Created')
                this.setState({
                    modal: !this.state.modal
                })
                window.location.reload()
            }) // parses JSON response into native JavaScript objects

    }
    deleteProduct = () => {
        fetch(`https://mp-binar-backend.herokuapp.com/product/${this.state.productId}`, {
            method: 'DELETE', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, cors, *same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
                // 'Content-Type': 'application/x-www-form-urlencoded',
                'authorization': `${localStorage.getItem('TOKEN')}`
            },
            redirect: 'follow', // manual, *follow, error
            referrer: 'no-referrer', // no-referrer, *client
            body: JSON.stringify({

            }), // body data type must match "Content-Type" header
        })
            .then(response => response.json())
            .then(data => {
                console.log(data);
                alert('Product Deleted')
                window.location.reload()



            }) // parses JSON response into native JavaScript objects

    }
    editProduct = () => {
        fetch(`https://mp-binar-backend.herokuapp.com/product/${this.state.productId}`, {
            method: 'PUT', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, cors, *same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
                // 'Content-Type': 'application/x-www-form-urlencoded',
                'authorization': `${localStorage.getItem('TOKEN')}`
            },
            redirect: 'follow', // manual, *follow, error
            referrer: 'no-referrer', // no-referrer, *client
            body: JSON.stringify({
                name:this.state.productName,
                price:this.state.productPrice
            }), // body data type must match "Content-Type" header
        })
            .then(response => response.json())
            .then(data => {
                console.log(data);
                alert('Product Updated')
                window.location.reload()
            }) // parses JSON response into native JavaScript objects

    }

    render() {
        return (
            <div>
                <Container style={{ marginTop: "5%" }}>
                    <Row>
                        <Col xs="3">
                            <Card>
                                <CardImg top width="100%"
                                    src={this.state.product.image ?
                                        this.state.product.image : `https://ui-avatars.com/api/?name=${this.state.product.name}`}
                                    alt="Card image cap" />
                                <CardBody>
                                    <CardTitle alt="username">{this.state.product.name}</CardTitle>
                                    <CardSubtitle alt="email">{this.state.product.price}</CardSubtitle>
                                    <CardText>Pelapak Terpercaya</CardText>
                                    <Modal isOpen={this.state.modal} toggle={this.modalToggle} className={this.props.className}>
                                        <ModalHeader toggle={this.toggle}>create Product</ModalHeader>
                                        <ModalBody>
                                            <Form>
                                                <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                                    <Label className="mr-sm-2">Product Name</Label>
                                                    <Input type="text" placeholder="Product Name" onChange={this.onChangeProductName} />
                                                </FormGroup>
                                                <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                                    <Label for="examplePassword" className="mr-sm-2" >Product price</Label>
                                                    <Input type="number" name="password" placeholder="password" onChange={this.onChangeProducPrice} />
                                                </FormGroup>
                                            </Form>
                                        </ModalBody>
                                        <ModalFooter>
                                            <Button color="primary" onClick={this.createProduct}>Create</Button>


                                            <Button color="secondary" onClick={this.modalToggle}>Cancel</Button>
                                        </ModalFooter>
                                    </Modal>
                                </CardBody>
                            </Card>
                        </Col>
                        <Col xs="9">
                            <Nav tabs>
                                <NavItem>
                                    <NavLink
                                        className={classnames({ active: this.state.activeTab === '1' })}
                                        onClick={() => { this.toggle('1'); }}>
                                        Account Setting</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink
                                        className={classnames({ active: this.state.activeTab === '2' })}
                                        onClick={() => { this.toggle('2'); }}>
                                        Your Product</NavLink>
                                </NavItem>
                            </Nav>
                            <TabContent activeTab={this.state.activeTab}>
                                <TabPane tabId="1">
                                    <Row>
                                        <Col sm="12">
                                            <Container>
                                                <Row>
                                                    <Col xs="2"></Col>
                                                    <Col xs="8">
                                                        <Form>
                                                            <Row form>
                                                                <Col md={6}>
                                                                    <FormGroup>
                                                                        <Label for="exampleUser">Username</Label>
                                                                        <Input disabled={this.state.isDisable} onChange={this.Onchangeusername} value={this.state.product.name} type="text" placeholder="Username" />
                                                                    </FormGroup>
                                                                </Col>
                                                                <Col md={6}>
                                                                    <FormGroup>
                                                                        <Label for="examplePassword">Password</Label>
                                                                        <Input disabled={this.state.isDisable} onChange={this.Onchangepassword} value={this.state.product.price} type="password" placeholder="password placeholder" />
                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>
                                                            <FormGroup>
                                                                <Label for="exampleEmail">Email</Label>
                                                                <Input disabled={this.state.isDisable} onChange={this.Onchangeemail} value={this.state.product.name} type="text" placeholder="email" />
                                                            </FormGroup>
                                                            <Button onClick={this.enable}>Edit</Button>
                                                            <Button onClick={this.edituser}>Save</Button>
                                                        </Form>
                                                    </Col>
                                                    <Col xs="2"></Col>
                                                </Row>
                                            </Container>
                                        </Col>
                                    </Row>
                                </TabPane>
                                <TabPane tabId="2">
                                    <Row>
                                        {this.state.reviewProduct.map((review, index) => (
                                            <Col sm="6">
                                                <Card body>

                                                    <CardTitle>{review.text}</CardTitle>
                                                    <CardText>{review.user}</CardText>
                                                    <Button onClick={() => { this.setState({ productId: review._id }); alert('delete product?'); this.deleteProduct(); console.log(this.state.productId) }}>Delete Product</Button>
                                                </Card>
                                            </Col>
                                        ))}
                                    </Row>

                                </TabPane>
                            </TabContent>
                        </Col>
                    </Row>
                </Container>


            </div>
        )
    }
    componentDidMount() {
        fetch(`https://mp-binar-backend.herokuapp.com/product/${this.props.match.params.id}`)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    product: data.data,
                    reviewProduct: [...this.state.reviewProduct ? this.state.reviewProduct : null]
                })
                console.log(this.state.product)
            })
    }
}