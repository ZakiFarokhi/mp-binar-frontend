import React from 'react';
import { Col, Row, Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';

export default class Example extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            email: '',
            image: ''
        }
    }
    render() {
        return (
            <Container>
                <Row>
                    <Col xs="2"></Col>
                    <Col xs="8">
                        <Form>
                            <Row form>
                                <Col md={6}>
                                    <FormGroup>
                                        <Label for="exampleUser">Username</Label>
                                        <Input onChange={this.Onchangeusername} value={this.state.username} type="text" placeholder="Username" />
                                    </FormGroup>
                                </Col>
                                <Col md={6}>
                                    <FormGroup>
                                        <Label for="examplePassword">Password</Label>
                                        <Input onChange={this.Onchangepassword} value={this.state.password} type="password" placeholder="password placeholder" />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <FormGroup>
                                <Label for="exampleEmail">Email</Label>
                                <Input onChange={this.Onchangeemail} value={this.state.email} type="text" placeholder="email" />
                            </FormGroup>
                            <Button onClick={this.postuser}>Sign Up</Button>
                        </Form>
                    </Col>
                    <Col xs="2"></Col>
                </Row>
            </Container>

        );
    }
    Onchangeusername = (event) => {
        this.setState({
            username: event.target.value
        })
    }
    Onchangepassword = (event) => {
        this.setState({
            password: event.target.value
        })
    }
    Onchangeemail = (event) => {
        this.setState({
            email: event.target.value
        })
    }
    postuser = () => {
        fetch('https://mp-binar-backend.herokuapp.com/user/', {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, cors, *same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrer: 'no-referrer', // no-referrer, *client
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password,
                email: this.state.email,
                image: "https://ui-avatars.com/api/?name=" + this.state.username
            }), // body data type must match "Content-Type" header
        })
            .then(response => response.json())
            .then(data => {
                console.log(data)
            }) // parses JSON response into native JavaScript objects
    }
}
